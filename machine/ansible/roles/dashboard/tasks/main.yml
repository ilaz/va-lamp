---
#
# INCLUDE OS VARS.
#
- name: Include OS variables.
  include_vars: "{{ ansible_distribution|lower }}.yml"

- name: Include OS version variables.
  include_vars: "{{ item }}"
  with_first_found:
    - "{{ ansible_distribution|lower }}{{ ansible_distribution_major_version }}.yml"
    - "{{ ansible_distribution|lower }}.yml"

#
# CREATE AND ENABLE TOOLS VHOST.
#
- name: Create tools vhost.
  template:
    src: "tools.conf.j2"
    dest: "{{ apache_conf_root_path }}/sites-available/tools.conf"
  notify: restart apache

- name: Enable the vhost.
  file:
    src: "{{ apache_conf_root_path }}/sites-available/tools.conf"
    dest: "{{ apache_conf_root_path }}/sites-enabled/tools.conf"
    state: link
  notify: restart apache

- name: Ensure that tools available directory exists.
  file:
    path: "/var/www/tools"
    state: directory
    owner: "{{ apache_owner }}"
    group: "{{ apache_group }}"
    mode: 0755

#
# CREATE AND ENABLE STATS VHOST.
#
- name: Create stats vhost.
  template:
    src: "stats.conf.j2"
    dest: "{{ apache_conf_root_path }}/sites-available/stats.conf"
  notify: restart apache

- name: Enable the vhost.
  file:
    src: "{{ apache_conf_root_path }}/sites-available/stats.conf"
    dest: "{{ apache_conf_root_path }}/sites-enabled/stats.conf"
    state: link
  notify: restart apache

- name: Ensure that stats directory exists.
  file:
    path: "/var/www/stats"
    state: directory
    owner: "{{ apache_owner }}"
    group: "{{ apache_group }}"
    mode: 0755

#
# COPY TOOLS FILES.
#
- name: Copy tools files.
  copy:
    src: "./tools/"
    dest: /var/www/tools/
    owner: "{{ apache_owner }}"
    group: "{{ apache_group }}"
    mode: 0755

#
# DOWNLOAD FONTAWESOME.
#
- name: Check if FontAwesome css exist.
  stat:
    path: "/var/www/tools/{{ dashboard_fontawesome_version }}/css/font-awesome.min.css"
  register: p

- name: Download FontAwesome.
  unarchive:
    src: "{{ dashboard_fontawesome_url }}"
    dest: "/var/www/tools"
    remote_src: True
    owner: "{{ apache_owner }}"
    group: "{{ apache_group }}"
    mode: 0755
  when: p.stat.exists == false

#
# DOWNLOAD JQUERY.
#
- name: Download jQuery.
  get_url:
    url: "{{ dashboard_jquery_url }}"
    dest: "/var/www/tools/jquery.min.js"

#
# DOWNLOAD ADMINER.
#
- name: Download adminer.
  get_url:
    url: "{{ dashboard_adminer_url }}"
    dest: "/var/www/tools/adminer.php"

#
# ENSURE THAT MEMCACHE DIRECTORY EXISTS.
#
- name: Ensure that memcache directory exists.
  file:
    path: "/var/www/stats/memcache"
    state: directory
    owner: "{{ apache_owner }}"
    group: "{{ apache_group }}"
    mode: 0755

#
# DOWNLOAD MEMCACHE ADMIN.
#
- name: Download Memcache Admin.
  unarchive:
    src: "{{ dashboard_memcache_admin_url }}"
    dest: "/var/www/stats/memcache/"
    remote_src: True
    owner: "{{ apache_owner }}"
    group: "{{ apache_group }}"
    mode: 0755

#
# DOWNLOAD OPCACHE ADMIN.
#
- name: Ensure that OPCache directory exists.
  file:
    path: "/var/www/stats/opcache"
    state: directory
    owner: "{{ apache_owner }}"
    group: "{{ apache_group }}"
    mode: 0755
  when: _php_version|float >= 5.6

- name: Download OPCache Admin.
  git:
    repo: "{{ dashboard_opcache_admin_url }}"
    dest: "/var/www/stats/opcache/"
  when: _php_version|float >= 5.6

#
# CREATE APC STATS ADMIN.
#
- name: Ensure that APC directory exists.
  file:
    path: "/var/www/stats/apc"
    state: directory
    owner: "{{ apache_owner }}"
    group: "{{ apache_group }}"
    mode: 0755
  when: _php_version|float < 5.6

- name: Copy APC stats file.
  copy:
    src: "{{ dashboard_apc_original_path }}"
    dest: "/var/www/stats/apc/apc.php"
    owner: "{{ apache_owner }}"
    group: "{{ apache_group }}"
    mode: 0755
  when: _php_version|float < 5.6

#
# CREATE INDEX.PHP.
#
- name: Create index.php.
  template:
    src: "index.php.j2"
    dest: "/var/www/html/index.php"
  notify: restart apache

#
# REMOVE APACHE INDEX.HTML.
#
- name: Remove apache index.html.
  file:
    state: absent
    path: "/var/www/html/index.html"