---
#
# INCLUDE OS VARS.
#
- name: Include OS variables.
  include_vars: "{{ ansible_distribution|lower }}.yml"

- name: Include OS version variables.
  include_vars: "{{ item }}"
  with_first_found:
    - "{{ ansible_distribution|lower }}{{ ansible_distribution_major_version }}.yml"
    - "{{ ansible_distribution|lower }}.yml"

#
# INSTALL APACHE AND PACKAGES.
#
- name: Install apache.
  package:
    pkg: "{{ apache_package }}"
    state: latest

- name: Install apache packages.
  package:
    pkg: "{{ item }}"
    state: latest
  with_items: "{{ apache_packages|default([]) }}"
  notify: restart apache

#
# CREATE VARIABLE WITH APACHE INSTALLED VERSION AND INCLUDE VARS FILES.
#
- name: Get installed version of Apache.
  shell: "{{ apache_package }} -v"
  changed_when: false
  check_mode: no
  register: _apache_version

- name: Create apache version variable.
  set_fact:
    _apache_version: "{{ _apache_version.stdout.split()[2].split('/')[1].split('.')[1] }}"

- include_vars: "apache-2{{ _apache_version }}.yml"

#
# ENSURE THAT SITES DIRECTORIES EXIST.
#
- name: Ensure that sites available directory exists.
  file:
    path: "{{ apache_conf_root_path }}/sites-available"
    state: directory
    mode: 0755

- name: Ensure that sites enabled directory exists.
  file:
    path: "{{ apache_conf_root_path }}/sites-enabled"
    state: directory
    mode: 0755

#
# CENTOS TASKS.
#
- name: Include Centos tasks.
  include: setup-centos.yml
  static: no
  when: (ansible_distribution == "CentOS")

#
# DEBIAN TASKS.
#
- name: Include Debian tasks.
  include: setup-debian.yml
  static: no
  when: (ansible_distribution == "Debian")

#
# CREATE VHOSTS.
#
- name: Create vhosts.
  template:
    src: "vhosts.conf.j2"
    dest: "{{ apache_conf_root_path }}/sites-available/{{ item.server_name }}.conf"
    owner: root
    group: root
    mode: 0644
  with_items: "{{ apache_vhosts|default([]) }}"
  notify: restart apache

- name: Enable vhosts.
  file:
    src: "{{ apache_conf_root_path }}/sites-available/{{ item.server_name }}.conf"
    dest: "{{ apache_conf_root_path }}/sites-enabled/{{ item.server_name }}.conf"
    state: link
  with_items: "{{ apache_vhosts|default([]) }}"
  notify: restart apache

- name: Add vhosts to the hosts file.
  lineinfile:
    dest: "/etc/hosts"
    regexp: "127.0.0.1 {{ item.server_name }}"
    line: "127.0.0.1 {{ item.server_name }}"
    state: present
  with_items: "{{ apache_vhosts|default([]) }}"

#
# CREATE SSL VHOSTS.
#
- name: Create SSL vhosts.
  template:
    src: "vhosts-ssl.conf.j2"
    dest: "{{ apache_conf_root_path }}/sites-available/{{ item.server_name }}-ssl.conf"
    owner: root
    group: root
    mode: 0644
  with_items: "{{ apache_vhosts_ssl|default([]) }}"

- name: Enable SSL vhosts.
  file:
    src: "{{ apache_conf_root_path }}/sites-available/{{ item.server_name }}-ssl.conf"
    dest: "{{ apache_conf_root_path }}/sites-enabled/{{ item.server_name }}-ssl.conf"
    state: link
  with_items: "{{ apache_vhosts_ssl|default([]) }}"
  notify: restart apache

- name: Add vhosts to the hosts file.
  lineinfile:
    dest: "/etc/hosts"
    regexp: "127.0.0.1 {{ item.server_name }}"
    line: "127.0.0.1 {{ item.server_name }}"
    state: present
  with_items: "{{ apache_vhosts_ssl|default([]) }}"

#
# CHECK SERVICE STATUS.
#
- name: Ensure apache has selected state and enabled on boot.
  service:
    name: "{{ apache_package }}"
    state: "{{ apache_state }}"
    enabled: yes